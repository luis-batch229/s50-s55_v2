import { Form, Button } from 'react-bootstrap';
import { useNavigate } from 'react-router-dom';
import { useState, useEffect, useContext, } from 'react';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function Register() {


	const { user } = useContext(UserContext);

	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');

	const [isActive, setIsActive] = useState(false);

	console.log(email);
	console.log(password1);
	console.log(password2);

	useEffect(() => {
		h
		if ((email !== '' && password1 !== '' && password2 !== '') && (password1 === password2)) {
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [email, password1, password2]);



	// function registerUser(e) {

	// 	e.preventDefault();

	// 	setEmail('');
	// 	setPassword1('');
	// 	setPassword2('');


	// }

	const registerUser = (e) => {
		e.preventDefault();
		fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
			method: "POST",
			body: JSON.stringify({
				email: email,
			})
		})
			.then(res => res.json())
			.then(data => {
				console.log(data);

				if (data === false) {
					fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
						method: "POST",
						headers: {
							"Content-type": "appplication.json"
						},
						body: JSON.stringify({
							firstName: firstName,
							lastName: lastName,
							email: email,
							mobileNo: mobileNo,
							password: password1
						})
					})

						.then(res => res.json())
						.then(data => {
							console.log(data);

							if (data === true) {
								Swal.fire({
									title: "Welcome",
									icon: 'success',
									text: "You have successfully registered! good job tho."

								})
								navigate("/login");
							} else {
								Swal.fire({
									title: "something went wrong tho.",
									icon: 'error',
									text: "please try again"

								})
							}

						})

				} else {
					Swal.fire({
						title: "Soemthing went really bad.",
						icon: 'error',
						text: "Email is already used!"
					})
				}
			})
	}

	return (

		(user.id !== null) ?
			<Navigate to="/courses" />
			:

			<Form onSubmit={(e) => registerUser(e)}>
				<Form.Group className="mb-3" controlId="userEmail">
					<Form.Label>Email address</Form.Label>
					<Form.Control type="email" placeholder="Enter email" value={email} onChange={e => setEmail(e.target.value)} required />
					<Form.Text className="text-muted">
						We'll never share your email with anyone else.
					</Form.Text>
				</Form.Group>

				<Form.Group className="mb-3" controlId="password1" >
					<Form.Label>Password</Form.Label>
					<Form.Control type="password" placeholder="Password" value={password1} onChange={e => setPassword1(e.target.value)} required />
				</Form.Group>

				<Form.Group className="mb-3" controlId="password2" >
					<Form.Label>Verify Password</Form.Label>
					<Form.Control type="password" placeholder="Verify Password" value={password2} onChange={e => setPassword2(e.target.value)} required />
				</Form.Group>

				{/*CONDITIONAL RENDERING -> IF ACTIVE BUTTON IS CLICKBLE -> IF INACTIVE BUTTON IS NOT CLIC*/}
				{
					(isActive) ?
						<Button variant="primary" type="submit" controlId="submitBtn">
							Register
						</Button>
						:
						<Button variant="primary" type="submit" controlId="submitBtn" disabled>
							Register
						</Button>
				}

			</Form>
	)
}